﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using CsQuery;
using System.Text;
using System.Threading.Tasks;

namespace MemeCollector.Models
{
    public delegate void ParseHandler(PostData result);
    
    class Receiver
    {
        const string upvoteClass = "_1rZYMD_4xY3gRcSS3p8ODO";
        const string mediaClass = "_2MkcR85HDnYngvlVW2gMMa";
        const string postClass = "scrollerItem";
        const string topDay = "top/?t=day";
        const string hot = "hot";
        const string domenName = "https://www.reddit.com";
        const string nameClass = "s56cc5r-0";
        
        const string imageClass = "media-element";
        readonly int minimum_upvotes = 1;
        readonly string subredditName;
        public event ParseHandler PageDone;
        WebClient client;
        string html;
        string concretePostHtml;
        private bool trigger = false;
        HttpWebRequest tryrequest;
        HttpWebResponse response;
        public Receiver(string redditLink)
        {
            subredditName = domenName + redditLink;
            try
            {
                tryrequest = (HttpWebRequest)WebRequest.Create(new Uri(subredditName));
                
                response = (HttpWebResponse)tryrequest.GetResponse();
                
            } catch {}
        }
        public Receiver(string redditLink, int minupvotes)
        {
            subredditName = domenName + redditLink;
            minimum_upvotes = minupvotes;
            try
            {
                tryrequest = (HttpWebRequest)WebRequest.Create(new Uri(subredditName));
                response = (HttpWebResponse)tryrequest.GetResponse();
            } catch {}
        }

        public void Begin()
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                PageDone(new PostData()
                {
                    Message = "Подключение успешно"
                });
                client = new WebClient();
                PreparePage(subredditName + hot);
            }
            else
            {
                PageDone(new PostData()
                {
                    Message = "Подключение не удалось"
                });
            }


        }
        void PreparePage(string redditLink)
        {
            client.DownloadStringAsync(new Uri(redditLink));
            client.DownloadStringCompleted += ClientStringDownloadCompleted;
            PageDone(new PostData()
            {
                Message = "Начало загрузки страницы"
            });
        }
        
        void ClientStringDownloadCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            html = e.Result;
            PageDone(new PostData()
            {
                Message = "Конец загрузки страницы html "
            });
            ParsePage(html);
            client.DownloadStringCompleted -= ClientStringDownloadCompleted;
            
        }


        public async void ParsePage(string page)
        {
            CQ cquery = CQ.Create(page);
            PageDone(new PostData()
            {
                Message = "Начало обработки html"
            });
            foreach (IDomObject obj in cquery.Find("div").Where(e => e.GetAttribute("class").Contains(postClass)))
            {
                PostData tempInfo = new PostData
                {
                    Upvotes = 0
                };
                await Task.Run(() => {
                    if (trigger)
                    {
                        trigger = false;
                        return;
                    }
                    PageDone.Invoke((new PostData()
                    {
                        Message = "Начало обработки поста"
                    }));
                    CQ childQuery = CQ.Create(obj);
                    CQ post = CQ.Create(childQuery.Find("div").Where(e => e.GetAttribute("class").Contains(upvoteClass)));
                    foreach (IDomObject child in post)
                    {
                        tempInfo.Upvotes = CountUpvotes(child.InnerText);
                        break;
                    }

                    if (!(tempInfo.Upvotes < minimum_upvotes))
                    {
                        foreach (IDomObject child in childQuery.Find("a").Where(e => e.GetAttribute("href").Contains(subredditName)))
                        {
                            tempInfo.Link = child.GetAttribute("href");
                            break;
                        }
                        foreach (IDomObject child in childQuery.Find("h2").Where(e => e.GetAttribute("class").Contains(nameClass)))
                        {
                            tempInfo.Name = child.InnerText;
                            break;
                        }
                        tempInfo.ImageLink = "";
                        tempInfo.ImageLink = GetMediaLink(tempInfo.Link, childQuery);
                      //  if (tempInfo.ImageLink != "")
                        PageDone(tempInfo);
                    }
                    PageDone.Invoke((new PostData()
                    {
                        Message = "Конец обработки поста"
                    }));
                    
                });
                if (trigger)
                {
                    trigger = false;
                    return;
                }

            }
            

        }

        string GetMediaLink(string postPage, CQ post)
        {
            string mediaLink = "";
            concretePostHtml = client.DownloadString(postPage);
            mediaLink = GetImageLink(post);
            if (mediaLink == "")
            {
                mediaLink = GetVideoLink(post);
                mediaLink = mediaLink == "" ? "Видео не нашло" : mediaLink;
            }
            return mediaLink;
        }

        string GetImageLink(CQ post)
        {
            string imageLink = "";
            foreach (IDomObject item in post.Find("img").Where(e => e.GetAttribute("class").Contains(imageClass)))
            {
                imageLink =  item.GetAttribute("src");
            }
            return imageLink;
        }

        string GetVideoLink(CQ post)
        {
            string videoLink = "";
            foreach (IDomObject item in post.Find("div").Where(e => e.GetAttribute("class").Contains("_2MkcR85HDnYngvlVW2gMMa")))
            {
                CQ temp = CQ.Create(item);
                videoLink += temp.Find("a").FirstOrDefault().GetAttribute("href");

                break;
            }
            if (videoLink == "")
            {
                foreach (IDomObject item in post.Find("video"))
                {
                    videoLink += "Видос найден";
                }
            }
            return videoLink;
        }

        float CountUpvotes(string text)
        {
            string newupvote = "";
            bool containsK = false;
            foreach (var c in text)
            {
                
                if (c == 'k')
                {
                    containsK = true;
                    break;
                }
                else if (c == '.')
                    newupvote += ',';
                else
                    newupvote += c;
            }
            if (!float.TryParse(newupvote, out float upvotes))
            {
                upvotes = 0;
            }
            if (containsK)
                upvotes *= 1000;
            return upvotes;
        }
    }

    public class PostData
    {
        public float Upvotes { get; set;}
        public string Name { get; set;}
        public string Link { get; set;}
        public string ImageLink { get; set;}
        public string Message { get; set;}
    }
}
