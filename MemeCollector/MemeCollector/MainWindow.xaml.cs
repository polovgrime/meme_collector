﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MemeCollector.Models;
using CsQuery;
using System.Net;
using System.IO;
using System.Windows.Threading;

namespace MemeCollector
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Receiver receiver;
        int posts;
        public MainWindow()
        {
            posts = 0;
            InitializeComponent();
            receiver = new Receiver(@"/r/gifs/", 10);
            TextBlock1.Text = "";
            receiver.PageDone += SetMessage;
            receiver.Begin();
        }
        
        void SetMessage(PostData message)
        {
            TextBlock1.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => UpdatePosts(message)));
        }

        void UpdatePosts(PostData item)
        {
            if (item.Message != null)
            {
                StatusBlock.Text = item.Message;
                return;
            }
            CountBlock.Text = $"Найдено постов: {++posts}";
            TextBlock1.Text += $"\nИмя поста: {item.Name}";
            TextBlock1.Text += $"\nСсылка на пост: {item.Link}";
            TextBlock1.Text += $"\nКоличество Upvotes: {item.Upvotes}";
            TextBlock1.Text += $"\nМедиа {item.ImageLink}";
            TextBlock1.Text += $"\n\n";
        }

        void FillText(List<PostData> results)
        {
            TextBlock1.Text = $"Найдено {results.Count} постов";
            
            foreach (var item in results)
            {
                TextBlock1.Text += $"\nИмя поста: {item.Name}";
                TextBlock1.Text += $"\nСсылка на пост: {item.Link}";
                TextBlock1.Text += $"\nКоличество Upvotes: {item.Upvotes}";
                TextBlock1.Text += $"\nМедиа {item.ImageLink}";
                TextBlock1.Text += $"\n\n";
            }
        }

        void FillTextBlock(HttpWebResponse response)
        {
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = null;
            if (response.CharacterSet == null)
            {
                readStream = new StreamReader(receiveStream);
            } else
            {
                readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
            }
            TextBlock1.Text = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
        }
    }
}
